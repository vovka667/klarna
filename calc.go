package main

import (
	"fmt"
	"math/big"
)

func inc(i *big.Int) *big.Int {
	return i.Add(i, big.NewInt(1))
}

func dec(i *big.Int) *big.Int {
	return i.Sub(i, big.NewInt(1))
}

//Makes a copy of *big.Int
func copy(i *big.Int) *big.Int {
	k := &big.Int{}
	return k.Set(i)
}

func fibonacci(n *big.Int) (*big.Int, error) {
	if n.Sign() == -1 {
		return nil, fmt.Errorf("'%s' is negative", n)
	}

	res := big.NewInt(0)
	next := big.NewInt(1)

	for i := big.NewInt(0); i.Cmp(n) < 0; inc(i) {
		res, next = next, res
		next.Add(next, res)
	}

	return res, nil
}

func ackermann(m, n *big.Int) (*big.Int, error) {
	if n.Sign() == -1 || m.Sign() == -1 {
		return nil, fmt.Errorf("Negative arguments are not allowed: m = '%s', n = '%s'", m, n)
	}

	if m.Cmp(big.NewInt(0)) == 0 {
		return inc(n), nil
	}

	if n.Cmp(big.NewInt(0)) == 0 {
		return ackermann(dec(copy(m)), big.NewInt(1))
	}

	n, err := ackermann(copy(m), dec(copy(n)))
	if err != nil {
		return n, err
	}

	return ackermann(dec(m), n)
}

func factorial(n *big.Int) (*big.Int, error) {
	if n.Sign() == -1 {
		return nil, fmt.Errorf("%s is negative", n)
	}

	res := big.NewInt(1)

	for n.Cmp(big.NewInt(0)) > 0 {
		res.Mul(res, n)
		dec(n)
	}

	return res, nil
}
