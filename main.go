package main

import (
	"html/template"
	"log"
	"math/big"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	counter = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "A counter for requests to the wrapped handler.",
		},
		[]string{"handler"},
	)

	duration = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_duration_seconds",
			Help:    "A histogram of latencies for requests.",
			Buckets: []float64{.25, .5, 1, 2.5, 5, 10},
		},
		[]string{"handler"},
	)
)

//Page is a type used in template for html page generation
type Page struct {
	Result   *big.Int
	Error    error
	Duration time.Duration
}

func handler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path

	timer := prometheus.NewTimer(duration.WithLabelValues(path))
	counter.WithLabelValues(path).Inc()

	p := &Page{}
	n := new(big.Int)
	m := new(big.Int)
	if value, ok := r.URL.Query()["n"]; ok {
		n.SetString(value[0], 0)
	}

	if value, ok := r.URL.Query()["m"]; ok {
		m.SetString(value[0], 0)
	}

	switch path {
	case "/fibonacci":
		p.Result, p.Error = fibonacci(n)
	case "/ackermann":
		p.Result, p.Error = ackermann(m, n)
	case "/factorial":
		p.Result, p.Error = factorial(n)
	}

	p.Duration = timer.ObserveDuration()

	t, err := template.ParseFiles("index.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = t.Execute(w, p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {
	http.HandleFunc("/", handler)
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}
