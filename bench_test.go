package main

import (
	"math/big"
	"testing"
)

func BenchmarkFibonacci(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := fibonacci(big.NewInt(int64(n)))
		if err != nil {
			b.Error(err)
		}
	}
}

func BenchmarkAckermann(b *testing.B) {
	for i := 0; i < b.N; i++ {
		// Execution of A(4, n) takes tooooo long
		for n := 0; n < 4; n++ {
			for m := 0; m <= n; m++ {
				_, err := ackermann(big.NewInt(int64(m)), big.NewInt(int64(n)))
				if err != nil {
					b.Error(err)
				}
			}
		}
	}
}

func BenchmarkFactorial(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := fibonacci(big.NewInt(int64(n)))
		if err != nil {
			b.Error(err)
		}
	}
}
