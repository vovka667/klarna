package main

import (
	"math/big"
	"testing"
)

func TestFibonacci(t *testing.T) {
	var tests = map[int64]uint64{
		0:  0,
		1:  1,
		2:  1,
		10: 55,
	}

	for n, expect := range tests {
		result, err := fibonacci(big.NewInt(n))
		if err != nil {
			t.Error(err)
		}

		if result.Uint64() != expect {
			t.Error(
				"For N =", n,
				"expected", expect,
				"got", result,
			)
		}
	}

	if _, err := fibonacci(big.NewInt(-1)); err == nil {
		t.Error("Fibonacci of negative number should return error")
	}
}

func TestAckermann(t *testing.T) {
	var tests = map[[2]int64]uint64{
		{0, 0}: 1,
		{1, 1}: 3,
		{3, 6}: 509,
	}

	for n, expect := range tests {
		result, err := ackermann(big.NewInt(n[0]), big.NewInt(n[1]))
		if err != nil {
			t.Error(err)
		}

		if result.Uint64() != expect {
			t.Error(
				"For N =", n,
				"expected", expect,
				"got", result,
			)
		}
	}

	if _, err := ackermann(big.NewInt(-1), big.NewInt(-1)); err == nil {
		t.Error("Ackermann function of negative numbers should return error")
	}
}

func TestFactorial(t *testing.T) {
	var tests = map[int64]uint64{
		0:  1,
		1:  1,
		10: 3628800,
	}

	for n, expect := range tests {
		result, err := factorial(big.NewInt(n))
		if err != nil {
			t.Error(err)
		}

		if result.Uint64() != expect {
			t.Error(
				"For N =", n,
				"expected", expect,
				"got", result,
			)
		}
	}

	if _, err := factorial(big.NewInt(-1)); err == nil {
		t.Error("Factorial of negative number should return error")
	}
}
