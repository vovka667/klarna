APP=klarna

test:
	go test -cover .

build: clean
	go build -o ${APP}

docker-build:
	docker build -t ${APP} .

run:
	go run main.go calc.go

docker-run: docker-build
	docker run -p 127.0.0.1:8080:8080 --rm ${APP}

clean:
	go clean
