# How to run

## Using `make` and **go compiler**

`make run`

## Using `make` and **docker**

`make docker-run`

## Using **Go compiler** only

go run main.go calc.go

## Using **docker** only

```
docker build -t klarna .
docker run -p 127.0.0.1:8080:8080 --rm klarna
```

# How to use

Follow the link http://localhost:8080

# How to monitor

All metrics in prometheus format are available here: http://localhost:8080/metrics
