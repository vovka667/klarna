package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
)

func TestHTTPRoot(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(handler))
	defer srv.Close()

	res, err := http.Get(srv.URL)

	if err != nil {
		t.Fatal(err)
	}

	if res.StatusCode != http.StatusOK {
		t.Errorf("status not OK")
	}
}

func TestHTTPFibonacci(t *testing.T) {
	urls := []string{
		"/fibonacci?n=1",
		"/ackermann?m=1&n=2",
		"/factorial?n=1",
	}
	srv := httptest.NewServer(http.HandlerFunc(handler))
	defer srv.Close()

	for _, url := range urls {
		res, err := http.Get(srv.URL + url)

		if err != nil {
			t.Fatal(err)
		}

		if res.StatusCode != http.StatusOK {
			t.Errorf("status not OK")
		}

		body, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}

		re := regexp.MustCompile("Result is:")
		if !re.Match(body) {
			t.Errorf("HTML page doesn't contain result")
		}
	}
}
